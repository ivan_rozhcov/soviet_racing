﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {

	//
	void  Start (){
	}
	//
	void  OnTriggerEnter ( Collider other  ){
		//Is it the Player who enters the collider?
		if (!other.CompareTag("Player")) 
			return; //If it's not the player dont continue

		//Is this transform equal to the transform of checkpointArrays[currentCheckpoint]?
		if (transform == Camera.main.GetComponent<CarCheckpoint>().checkPointArray[CarCheckpoint.currentCheckpoint].transform) {
			//Check so we dont exceed our checkpoint quantity
			if (CarCheckpoint.currentCheckpoint + 1<Camera.main.GetComponent<CarCheckpoint>().checkPointArray.Length) {
				//Add to currentLap if currentCheckpoint is 0
				if(CarCheckpoint.currentCheckpoint == 0){
					CarCheckpoint.currentLap++;
					Camera.main.GetComponentInChildren<LapCounter>().decLap();
				}
				CarCheckpoint.currentCheckpoint++;
			} else {
				//If we dont have any Checkpoints left, go back to 0
				CarCheckpoint.currentCheckpoint = 0;
			}
			visualAid(); //Run a coroutine to update the visual aid of our Checkpoints
			//Update the 3dtext
			Camera.main.GetComponentInChildren<TextMesh>().text = "Checkpoint: "+(CarCheckpoint.currentCheckpoint)+" Lap: "+(CarCheckpoint.currentLap);
			Camera.main.GetComponentInChildren<Timer>().reset();
		}
	}
	//
	void  visualAid (){
		//Set a simple visual aid for the Checkpoints
		foreach(Transform objAlpha in Camera.main.GetComponent<CarCheckpoint>().checkPointArray) {
			Color other = new Color(0.777f, 08, 0.604f);
			other.a = 1f;
			objAlpha.GetComponent<Renderer>().material.color = other;
		}
		Color next = new Color(250, 128, 114);
		Camera.main.GetComponent<CarCheckpoint>().checkPointArray[CarCheckpoint.currentCheckpoint].GetComponent<Renderer>().material.color = next;
//		sim.a =  0.8f;
	}
}