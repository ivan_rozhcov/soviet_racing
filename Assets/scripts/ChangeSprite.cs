﻿using UnityEngine;
using System.Collections;




public class ChangeSprite : MonoBehaviour {
	public float offset = 11.25f;
	public Sprite north, northEast, east, southEast, south, southWest, west, northWest;
	public GameObject cameraObject;
	// Use this for initialization
	void Start () {
		SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
		spriteRenderer.sprite = north;	
		cameraObject = GameObject.Find("Main Camera");
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 v2Forward = new Vector2 (transform.parent.forward.x, transform.parent.forward.z);
//		Vector2 v2Camera = new Vector2 (Camera.transform.forward.x, cameraObject.transform.forward.z);
		Vector2 v2Camera = new Vector2 (cameraObject.transform.forward.x, cameraObject.transform.forward.z);
		float angle = Vector2.Angle(v2Forward, v2Camera);
		Vector3 cross = Vector3.Cross(v2Forward, v2Camera);
		if (cross.z > 0)
			angle = 360 - angle;
		angle = angle + offset;
		SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

//		пришлось поменять метсами спрайты, так как разворячивает лицом к камере
		if (0 < angle && angle < 45){
			spriteRenderer.sprite = north;
		}
		else if (45 <= angle && angle < 90){
//			spriteRenderer.sprite = northEast;
			spriteRenderer.sprite = northWest;
		}
		else if (90 <= angle && angle < 135){
//			spriteRenderer.sprite = east;
			spriteRenderer.sprite = west;
		}
		else if (135 <= angle && angle < 180){
//			spriteRenderer.sprite = southEast;
			spriteRenderer.sprite = southWest;
		}
		else if (180 <= angle && angle < 225){
			spriteRenderer.sprite = south;
		}
		else if (225 <= angle && angle < 270){
//			spriteRenderer.sprite = southWest;
			spriteRenderer.sprite = southEast;
		}	
		else if (270 <= angle && angle < 315){
//			spriteRenderer.sprite = west;
			spriteRenderer.sprite = east;
		}
		else if (315 <= angle && angle < 360){
//			spriteRenderer.sprite = northWest;
			spriteRenderer.sprite = northEast;
		}

//		transform.LookAt(Camera.main.transform.position, Vector3.up);

//		http://answers.unity3d.com/questions/715748/sprites-always-face-the-camera-camera-freely-rotat.html
//		Vector3 oppositeCamera = transform.position - Camera.main.transform.position;
//		Quaternion faceCamera = Quaternion.LookRotation(oppositeCamera);
//		Vector3 euler = faceCamera.eulerAngles;
//		euler.y = 0f;
//		faceCamera.eulerAngles = euler;
//		transform.rotation = faceCamera;
		transform.rotation = Camera.main.transform.rotation;
	}
}
