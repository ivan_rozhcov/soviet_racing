﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {


	float timeLeftInitial = 15.0f;
	float timeLeft = 15.0f;
	// Use this for initialization
	void Start () {
	
	}
	public void reset(){
		timeLeft = timeLeftInitial;
	}
	// Update is called once per frame
	void Update () {
		timeLeft -= Time.deltaTime;
		gameObject.GetComponent<TextMesh>().text = "Timer: " + timeLeft;
		if(timeLeft < 0)
		{
			Camera.main.GetComponentInChildren<TextMesh>().text = "Game Over! Time's up!";
			UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
		}
	}
}
