﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class CarCheckpoint : MonoBehaviour {


public Transform[] checkPointArray; //Checkpoint GameObjects stored as an array
public static int currentCheckpoint = 0; //Current checkpoint
public static int currentLap = 0; //Current lap
static Vector3 startPos; //Starting position

void  Start (){
	//Set a simple visual aid for the Checkpoints

	foreach(Transform objAlpha in checkPointArray) {
		Color sim = objAlpha.GetComponent<Renderer>().material.color;
		sim = new Color(0.777f, 08, 0.604f);
		sim.a = 0.2f;
	}
	Color sim1  = checkPointArray[0].GetComponent<Renderer>().material.color;
	sim1.a = 0.8f;
	
	//Store the starting position of the player
	startPos = transform.position;
}

}