﻿using UnityEngine;
using System.Collections;

public class LapCounter : MonoBehaviour {
	public int lapsCounter = 2;
	public int lapsAll;
	// Use this for initialization
	void Start () {
		int lapsAll = lapsCounter;
	}
	
	// Update is called once per frame
	void Update () {
		if(lapsCounter < 0)
		{
			Camera.main.GetComponentInChildren<TextMesh>().text = "Game Over! You've finished!";
			UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
		}
	}
	public void decLap(){
		lapsCounter = lapsCounter - 1;
	}
}
