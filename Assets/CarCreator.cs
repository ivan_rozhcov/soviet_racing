﻿using UnityEngine;
using System.Collections;

public class CarCreator : MonoBehaviour {
	public GameObject[] cars;
	public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.

	// Use this for initialization
	void Start () {
		// Instantiate the missile at the position and rotation of this object's transform
		GameObject car = (GameObject)Instantiate (cars[0], spawnPoints[0].position, spawnPoints[0].rotation);
		// 1st car is player
		car.tag = "Player";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
